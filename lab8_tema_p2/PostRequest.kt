package com.pp.laborator

import khttp.responses.Response

class PostRequest(url: String, params: Map<String, String>?) {

    var genericRequest: GenericRequest? = null

    init {
        genericRequest = GenericRequest(url, params)
    }

    fun setParams(URL: String, map: Map<String, String>) {
        this.genericRequest!!.url = URL
        this.genericRequest!!.params = map
    }

}