package com.pp.laborator

import khttp.responses.Response

class GetRequest(url: String, params: Map<String, String>?, timeout: Double) : HTTPGet {
    var timeout: Double = 0.0
    var genericRequest: GenericBrowserRequest? = null
    init {
        genericRequest = GenericRequest(url, params)
        this.timeout = timeout
    }
    fun setURL(URL: String) {
        genericRequest!!.url = URL
    }
    fun getURL(): String {
        return genericRequest!!.url
    }
}