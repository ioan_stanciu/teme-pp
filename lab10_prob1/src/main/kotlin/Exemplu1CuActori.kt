package com.pp.laborator


import kotlinx.coroutines.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.system.*
import kotlin.system.*
import kotlinx.coroutines.channels.*

sealed class ContorMsg
object IncContor : ContorMsg()
class GetContor(val response: CompletableDeferred<Int>) : ContorMsg()

fun CoroutineScope.counterActor() = actor<ContorMsg> {
    var contor = 0
    for (msg in channel) {
        when (msg) {
            is IncContor -> contor++
            is GetContor -> msg.response.complete(contor)
        }
    }
}

suspend fun CoroutineScope.massiveRun2(action: suspend () -> Unit) {
    val n = 100
    val k = 1000
    val time = measureTimeMillis {
        val jobs = List(n){
            launch { repeat(k) { action() } }
        }
        jobs.forEach { it.join() }
    }
    println("S-au efectuat ${n * k} operatii in $time ms")
}

val mtContext2 = newFixedThreadPoolContext(2, "mtPool")
var counter2 = AtomicInteger(0)
var result2=0

fun main() = runBlocking<Unit> {
    val contor = counterActor()
    GlobalScope.massiveRun(){
        contor.send(IncContor)
    }
    val raspuns = CompletableDeferred<Int>()
    contor.send(GetContor(raspuns))
    println("Contor = ${raspuns.await()}")
    contor.close()
}
