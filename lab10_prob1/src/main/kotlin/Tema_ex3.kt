package com.pp.laborator

import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

suspend fun createCoroutines(nr: Int, maxim: Int) {
    val jobs = ArrayList<Job>()
    for (i in 1..nr) {
        jobs += GlobalScope.launch {
            var result: Int = 0
            val x = (1..maxim).shuffled().first()
            for (j in 1..x){
                result += j
            }
            println( "[$i] Nr: " + x.toString() + " +rez: " + result)
        }
    }
    jobs.joinAll()
}

fun main() = runBlocking{
    val time = measureTimeMillis {
        createCoroutines(8,20)
    }
    println("timp: " + time + " [ms]")
}

