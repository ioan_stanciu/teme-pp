package com.pp.laborator


import kotlinx.coroutines.*
import java.io.File
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.locks.ReentrantLock
import kotlin.system.*

class Lista(filePath: String) {
    private val cLock = ReentrantLock()
    private var filePath: String? = null;
    init {
        this.filePath = filePath
    }
    @Volatile
    public var container: MutableList<Int> = mutableListOf()
    fun append(value: Int) {
        try {
            cLock.lock()
            container.add(value)
            File(filePath).appendText(value.toString())
        } finally {
            cLock.unlock()
        }
    }
}

suspend fun CoroutineScope.massiveRun(action: suspend () -> Unit) {
    val n = 100
    val k = 1000
    val numbers= mutableListOf(1,2,3,4);
    val time = measureTimeMillis {
        val jobs = List(n){
            launch { repeat(k) { action() } }
        }
        jobs.forEach { it.join() }
    }
    println("S-au efectuat ${n * k} operatii in $time ms")
}

val mtContext = newFixedThreadPoolContext(2, "mtPool")
var counter = AtomicInteger(0)
var result=0
fun main() = runBlocking<Unit> {
    val Lista: Lista = Lista("ex.out")

    CoroutineScope(mtContext).massiveRun {

        result = counter.incrementAndGet() //variabila comuna unde vor aparea erori
        Lista.append(counter.get())
    }
    println("Numarator = $result")
    for (i in Lista.container) {
        print("$i ")
    }

}
