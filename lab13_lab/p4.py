import math

if __name__=='__main__':
	n= int(input('Patrat max: ').strip())
	patrate = (i*i for i in range(int(math.sqrt(n))))
	res=filter(lambda x: x<n and x%2 == 0, patrate)
	
	for i in res:
		print(i, end=' ')
