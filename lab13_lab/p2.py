class str(str):
	def to_pascal_case(self):
		words=self.strip().split(' ')
		return ''.join([w.capitalize() for w in words])


if __name__=='__main__':
	print(str("am pastile").to_pascal_case())