import types

def decorator_cu_argumente(replacement_fct=None):
	def wrap(f):
		def wrapped_f(*args, **kwargs):
			if replacement_fct and isinstance(replacement_fct, types.FunctionType):
				output=replacement_fct(*args, **kwargs)
			else:
				output=f(*args, **kwargs)
			return output

		return wrapped_f

	return wrap


def replacement(self):
	print("Replacement")
	for i in range(2, int(self**0.5)+1):
		if self % i == 0:
			return False

	return True


class int(int):
	@decorator_cu_argumente(replacement_fct = replacement)
	def is_prime(self):
		for i in range(2, int(self**0.5)+1):
			if self % i == 0:
				return False

		return True

if __name__=='__main__':
	print("Ret: ", int(7).is_prime())