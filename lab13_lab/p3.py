import functools
from functional import seq
import itertools

def zip(*args):
	zip_len=min(len(arg) for arg in args)
	res=seq(range(zip_len)).map(lambda it: tuple(arg[it] for arg in args))
	for it in res:
		yield it


if __name__=='__main__':
	x=[1,2,3]
	y=['a', 'b', 'c', 'd']
	a=zip(x,y)
	for i in a:
		print(i, end=' ')