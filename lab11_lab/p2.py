from threading import Thread, Condition
import time
import multiprocessing
elemente = []
conditie = Condition()


class Consumator(multiprocessing.Process):
    def __init__(self, queue):
        multiprocessing.Process.__init__(self)
        self.queue=queue

    def consumator(self):
        global conditie
        global elemente
        conditie.acquire()
        if len(elemente) == 0:
            conditie.wait()
            print('mesaj de la consumator: nu am nimic disponibil')
        elemente.pop()
        print('mesaj de la consumator : am utlizat un element')
        print('mesaj de la consumator : mai am disponibil', len(elemente),
              'elemente')
        conditie.notify()
        conditie.release()

    def run(self):
        for i in range(5):
            self.consumator()


class Producator(multiprocessing.Process):
    def __init__(self,queue):
        multiprocessing.Process.__init__(self)
        self.queue=queue

    def producator(self):
        global conditie
        global elemente
        conditie.acquire()
        if len(elemente) == 10:
            conditie.wait()
            print('mesaj de la producator : am disponibile', len(elemente),
                  'elemente')
            print('mesaj de la producator : am oprit productia')
        elemente.append(1)
        print('mesaj de la producator : am produs', len(elemente), 'elemente')
        conditie.notify()
        conditie.release()

    def run(self):
        for i in range(5):
            self.producator()


if __name__ == '__main__':
    queue=multiprocessing.Queue()
    producator = Producator(queue)
    consumator = Consumator(queue)
    producator.start()
    consumator.start()
    producator.join()
    consumator.join()