import asyncio
import sys
import queue
from random import *

@asyncio.coroutine
def first_coroutine(future, num):
    count = 0
    for i in range(1, num + 1):
        count += i
    yield from asyncio.sleep(4)
    future.set_result(f'(sum of {num} ints) result={count}')

def got_result(future):
    print(future.result())


if __name__ == '__main__':
    q = queue.Queue()
    for i in range (0,4):
        q.put(randint(0,1000))

    numbers=[]
    for i in range(0,4):
        numbers.append(q.get())
    loop = asyncio.get_event_loop()
    future=[]
    tasks=[]
    for i in range (0,4):
        future.append(asyncio.Future())
        tasks.append(first_coroutine(future[i], numbers[i]))
        future[i].add_done_callback(got_result)
    
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()