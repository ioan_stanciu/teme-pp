if __name__ == '__main__':
    my_list = [1, 21, 75, 39, 7, 2, 35, 3, 31, 7, 8]

    my_list = list(filter(lambda x: x > 5, my_list))
    print(my_list)

    my_list = list(zip(*[iter(my_list)] * 2))
    print(my_list)

    my_list = list(map(lambda x: x[0] * x[1], my_list))
    print(my_list)

    print(sum(my_list))