import os

class GenericFile:
    def get_path(self):
        raise NotImplementedError("Error")

    def get_freq(self):
        raise NotImplementedError("Error")

class TextASCII(GenericFile):
    def __init__(self, path_absolut, frecvente):
        self.path_absolut = path_absolut
        self.frecvente = frecvente
    def get_path(self):
        return self.path_absolut

    def get_freq(self):
        return self.frecvente

class TextUNICODE(GenericFile):
    def __init__(self, path_absolut, frecvente):
        self.path_absolut = path_absolut
        self.frecvente = frecvente
    def get_path(self):
        return self.path_absolut

    def get_freq(self):
        return self.frecvente

class Binary(GenericFile):
    def __init__(self, path_absolut, frecvente):
        self.path_absolut = path_absolut
        self.frecvente = frecvente
    def get_path(self):
        return self.path_absolut

    def get_freq(self):
        return self.frecvente

class XMLFile(TextASCII):
    def __init__(self, first_tag, path_absolut, frecvente):
        super().__init__(path_absolut, frecvente)
        self.first_tag=first_tag

    def get_first_tag(self):
        return self.first_tag

class BMP(Binary):
    def __init__(self, width, height, bpp, path_absolut, frecvente):
        super().__init__(path_absolut, frecvente)
        self.width=width
        self.height=height
        self.bpp=bpp
        
    def show_info(self):
        print([self.width, " ", self.height, " ", self.bpp])

def get_type_of_file(freq):
    count_zero=freq[0]
    total=0
    for i in freq:
        total+=i
    if (float(count_zero))/total >= 0.3:
        return "unicode"

    contor_mid=0
    contor_cap=0
    for i in range (9, 126):
        contor_mid+=freq[i]

    contor_cap=total-contor_mid

    if contor_mid> 5*contor_cap: #am asumat ca aceasta constanta 5 inseamna mult mai mari
        return "ascii"

    return "binary"


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
list_ascii=[]
list_xml=[]
list_unicode=[]
list_binary=[]
list_bmp=[]

OFFSET_WIDTH=18
OFFSET_HEIGHT=22
OFFSET_BPP=28

def parcurgere(DIR):
    for root, subdirs, files in os.walk(DIR):
        for file in os.listdir(root):
            file_path = os.path.join(root, file)
            if os.path.isfile(file_path):
                # deschide fișierul spre acces binar
                f = open(file_path, 'rb')
                try:
                    # în content se va depune o listă de octeți
                    content = f.read()
                    print(content)
                    freq=[]
                    for i in range (0,256):
                        freq.append(0)
                    for octet in content:
                        freq[octet]=freq[octet]+1
                    result=get_type_of_file(freq)
                    if result == "ascii":
                        list_ascii.append(TextASCII(file_path, freq))
                        #for i in range (0, len(content)-2):
                         #   if content[i]==ord('x') and content[i+1] ==ord('m') and content[i+2] ==ord('l'):
                          #          list_xml.append(XMLFile("", file_path, freq))
                           #         break
                        if content[0]==ord('<') and content[1]==ord('?') and content[2]==ord('x') and content[3]==ord('m') and content[4]==ord('l'):
                            list_xml.append(XMLFile("", file_path, freq))
                    if result == "unicode":
                        list_unicode.append(TextUNICODE(file_path, freq))
                    if result == "binary":
                        list_binary.append(Binary(file_path, freq))
                        if content[0]== 66 and content[1]==77: #BMP
                            width=content[OFFSET_WIDTH]*pow(2,24)+content[OFFSET_WIDTH+1]*pow(2,16)+content[OFFSET_WIDTH+2]*pow(2,8)+content[OFFSET_WIDTH+3]
                            height=content[OFFSET_HEIGHT]*pow(2,24)+content[OFFSET_HEIGHT+1]*pow(2,16)+content[OFFSET_HEIGHT+2]*pow(2,8)+content[OFFSET_HEIGHT+3]
                            bpp=content[OFFSET_BPP]*pow(2,8)+content[OFFSET_BPP+1]
                            list_bmp.append(BMP(width,height,bpp,file_path, freq))
                finally:
                    f.close()
        else:
            parcurgere(file_path)

def print_paths(ob):
    for i in ob:
        print(i.get_path())

if __name__ == '__main__':
    parcurgere(ROOT_DIR)
    print("Fisiere ASCII: ")
    print_paths(list_ascii)
    print("Fisiere XML: ")
    print_paths(list_xml)
    print("Fisiere BMP: ")
    print_paths(list_bmp)
    for b in list_bmp:
        b.show_info()
    print("Fisiere UNICODE: ")
    print_paths(list_unicode)
    print("Fisiere binary: ")
    print_paths(list_binary)