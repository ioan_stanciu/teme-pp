import threading
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
import time
import random

vec=[]
for i in range (0,3000000):
	vec.append(random.randint(1,1000))


def countdown11():
   local_vec=vec
   local_vec.sort()

def isPrime(n) : 
    if (n <= 1) : 
        return False
    if (n <= 3) : 
        return True
    if (n % 2 == 0 or n % 3 == 0) : 
        return False
    i = 5
    while(i * i <= n) : 
        if (n % i == 0 or n % (i + 2) == 0) : 
            return False
        i = i + 6
    return True

def filtrare():
	prime_numbers=[]
	for elem in vec:
		if isPrime(elem):
			prime_numbers.append(elem)


def flitrare_cu_arg(arr):
	prime_numbers=[]
	for elem in arr:
		if isPrime(elem):
			prime_numbers.append(elem)

def ridicare_la_putere(arr):
	X=10
	for i in range (0,3000000):
		a=arr[i]
		for j in range (0,X):
			arr[i]*=a

def increment_arr(arr):
	for i in range (0, len(arr)):
		arr[i]+=1


def ver_1():
    thread_1 = threading.Thread(target=filtrare)
    thread_2 = threading.Thread(target=filtrare)
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()


def ver_2():
    filtrare()
    filtrare()


def ver_3():
    process_1 = multiprocessing.Process(target=filtrare)
    process_2 = multiprocessing.Process(target=filtrare)
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()


def ver_4():
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(filtrare())
        future = executor.submit(filtrare())

def ver_1_param(arr):
    thread_1 = threading.Thread(target=increment_arr, args=(arr, ))
    thread_2 = threading.Thread(target=increment_arr, args=(arr, ))
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()


def ver_2_param(arr):
    increment_arr(arr)
    increment_arr(arr)


def ver_3_param(arr):
    process_1 = multiprocessing.Process(target=increment_arr, args=(arr, ))
    process_2 = multiprocessing.Process(target=increment_arr, args=(arr, ))
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()


def ver_4_param(arr):
    with ThreadPoolExecutor(max_workers=2) as executor:
        future = executor.submit(increment_arr(arr))
        future = executor.submit(increment_arr(arr))


if __name__ == '__main__':
    start = time.time()
    ver_1_param(vec)
    end = time.time()
    print("\n Timp executie pseudoparalelism cu GIL")
    print(end - start)
    start = time.time()
    ver_2_param(vec)
    end = time.time()
    print("\n Timp executie secvential")
    print(end - start)
    start = time.time()
    ver_3_param(vec)
    end = time.time()
    print("\n Timp executie paralela cu multiprocessing")
    print(end - start)
    start = time.time()
    ver_4_param(vec)
    end = time.time()
    print("\n Timp executie paralela cu concurrent.futures")
    print(end - start)