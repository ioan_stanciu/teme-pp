import java.sql.Timestamp

class HistoryLogRecord (var timp: Timestamp, var command:String): Comparable<HistoryLogRecord>{
    override fun toString(): String {
        return String.format("Date: "+timp+"\nCommandline: "+command)
    }
    override fun compareTo(a: HistoryLogRecord):Int{
        if(timp.year>a.timp.year)
            return 1;
        if(timp.year<a.timp.year)
            return -1;

        if(timp.month>a.timp.month)
            return 1;
        if(timp.month<a.timp.month)
            return -1;

        if(timp.date>a.timp.date)
            return 1;
        if(timp.date<a.timp.date)
            return -1;

        if(timp.hours>a.timp.hours)
            return 1;
        if(timp.hours<a.timp.hours)
            return -1;

        if(timp.minutes>a.timp.minutes)
            return 1;
        if(timp.minutes<a.timp.minutes)
            return -1;

        if(timp.seconds>a.timp.seconds)
            return 1;
        if(timp.seconds<a.timp.seconds)
            return -1;

        return 0;
    }


}