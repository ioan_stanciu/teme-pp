import kotlin.math.sqrt

fun main() {
    val pairs = listOf<Int>(3,5,34,3,5) zip listOf<Int>(4,22,3,31,55)
    val firstPair = pairs.get(0)
    val lastPair = pairs.get(pairs.size - 1)
    val distList = (pairs.zipWithNext { pair1, pair2 ->
        sqrt(((pair1.first - pair2.first) * (pair1.first - pair2.first) + (pair1.second - pair2.second) * (pair1.second - pair2.second)).toDouble()) }.toMutableList() + (
            ((lastPair.first - firstPair.first) * (lastPair.first - firstPair.first) + (lastPair.second - firstPair.second) * (lastPair.second - firstPair.second)).toDouble()))

    print(distList.sum())

}

