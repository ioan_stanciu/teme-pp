fun main()
{
    val firstList = mutableListOf<Int>(1,21,75,39,7,2,35,3,31,7,8)
    val result = firstList.asSequence().filter { it > 5 }.zipWithNext{x,y -> x*y}.filterIndexed{ id, value -> id % 2 == 0}.toMutableList().sum()
    println(result)


}