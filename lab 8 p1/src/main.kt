import factory.FactoryProducer

fun main(args: Array<String>) {

    val factory = FactoryProducer()
    val eliteFactory = factory.getFactory("EliteFactory")
    val happyWorkerFactory = factory.getFactory("HappyWorkerFactory")

    var ceo1 = eliteFactory?.getHandler("CEOHandler")
    var ceo2 = eliteFactory?.getHandler("CEOHandler")

    var executive1 = eliteFactory?.getHandler("ExecutiveHandler")
    var executive2 = eliteFactory?.getHandler("ExecutiveHandler")

    var manager1 = eliteFactory?.getHandler("ManagerHandler")
    var manager2 = eliteFactory?.getHandler("ManagerHandler")

    var worker1 = happyWorkerFactory?.getHandler("HappyWorkerHandler")
    var worker2 = happyWorkerFactory?.getHandler("HappyWorkerHandler")
}