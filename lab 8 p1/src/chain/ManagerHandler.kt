package chain
class ManagerHandler(var next1: Handler? = null, var next2: Handler? =null): Handler {
    override fun handleRequest(forwardDirection: String, messageToBeProcessed: String) {
        val messages = messageToBeProcessed.split(":");
        if (messages[0] == "3") {
            print("Manager" + messages[1]);
            return;
        }
        when (forwardDirection) {
            "Down" -> next1?.handleRequest(forwardDirection, messageToBeProcessed);
            "Right" -> next2?.handleRequest(forwardDirection, messageToBeProcessed);
        }
    }
}