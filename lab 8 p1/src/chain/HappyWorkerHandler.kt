package chain

class HappyWorkerHandler(var next1: Handler? = null, var next2:Handler? = null): Handler {
    override fun handleRequest(forwardDirection: String,messageToBeProcessed: String) {
        val messages = messageToBeProcessed.split(":");
        if(messages[0]== "4") {
            if(forwardDirection == "Down"){
                if(next1!=null) {
                    next1?.handleRequest("Up", messages[1]);
                }
                else
                    print("Happy Worker" + messages[1]);
            }
        }
        else {
            next2?.handleRequest(forwardDirection, messageToBeProcessed);
        }
    }
}