package factory

import chain.CEOHandler
import chain.ExecutiveHandler
import chain.Handler
import chain.ManagerHandler

class EliteFactory: AbstractFactory() {
    override fun getHandler(handler: String): Handler? {
        return when(handler){
            "ManagerHandler" -> ManagerHandler()
            "CEOHandler" -> CEOHandler()
            "ExecutiveHandler" -> ExecutiveHandler()
            else -> null
        }
    }
}